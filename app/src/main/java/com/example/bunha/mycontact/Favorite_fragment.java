package com.example.bunha.mycontact;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.List;


public class Favorite_fragment extends Fragment implements TextWatcher {
    private static final String ARG_TITLE = "title";
    private String title;

    List<Person> personList;
    private PersonDbHelper personDbHelper;
    private PersonAdapter adapter;
    Context context;

    EditText etSearchByName;
    private RecyclerView recyclerView;
public  Favorite_fragment (){

}

    public  Favorite_fragment newInstance(String title) {
        Favorite_fragment fragment = new Favorite_fragment();
        fragment.context=context;
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_TITLE);
        }

        personDbHelper = new PersonDbHelper(this.getContext());
        adapter = new PersonAdapter(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rvPerson);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        etSearchByName = (EditText) view.findViewById(R.id.etSearchByName);
        etSearchByName.addTextChangedListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        personList = personDbHelper.findAllPersons();
        adapter.clear();
        adapter.addPerson(personList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        List<Person> list = personDbHelper.findPersonByName(String.valueOf(s));
        adapter.clear();
        adapter.addPerson(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

}
